/*!
 * require.mustache - 0.0.1
 * https://www.simpletechs.net
 * Copyright (c) 2013 simpleTechs.net (fabian@simpletechs.net)
 * Licensed TBD
 * Uses: canjs (http://canjs.us/)
 * State: IN PRODUCTION
 */

define(function() {
	var Mustache = {},
		buildMap = {};

	Mustache.load = function(name, parentRequire, load, config) {
		var path = parentRequire.toUrl(name + '.mustache');

		if(config.isBuild) {
			var compiler = require.nodeRequire('can-compile');

			compiler.compile({filename: path, version: '2.1.3'}, function(error, output) {
				//TODO: this is a hacky solution, we should totally get this straight someday
				output = "require(['can/view/mustache'], function(can) { can.view.preloadStringRenderer('" + name.replace(/[^a-zA-Z0-9]+/g, '_') + "_mustache', " + output + "); })\n";
				buildMap[name] = output;
				load(output);
			});
		}
		else {
			parentRequire(['can/view/mustache'], function(can) {
				load(function(data){
					var args = can.makeArray(arguments);
					args.unshift(path);

					return can.view.apply(this, args);
				});
			});
		}
	};

	Mustache.write = function (pluginName, name, write) {
		if (buildMap.hasOwnProperty(name)) {
			var text = buildMap[name];
			write.asModule(pluginName + "!" + name, text);
		}
	};

	return Mustache;

});