/*!
 * can.chosen - 0.0.1
 * https://www.simpletechs.net
 * Copyright (c) 2013 simpleTechs.net (fabian@simpletechs.net)
 * Licensed TBD
 * Uses: canjs (http://canjs.us/), jQuery (http://jquery.com)
 * State: IN TESTING
 */

define([
        'jquery',
        'can',
        'mustache!./views/404'
    ],
function($, can, View404) {
    // define some routes
    // can.route.ready(false);
    can.route('/:view');
    can.route('/:view/:id');
    // can.route('/:view/edit/:id');
    can.route('/:view/:action/:id');

    var userAccess = window.uA = can.compute('public'),
        savedRoute,
        activeModule;

    return can.Control.extend({
        
    },{
        _initRoutes: function(access, routes, loadedModules, contentElement, options) {
            //setup routes
            can.each(this.constructor.modules[access], function(module, route) {
                if(route === 'default') return;

                if(!module.pluginName) {
                    module.pluginName = module.fullName || module.shortName || ('Module-'+Math.round(Math.random() * 1000));
                }
                console.log('[Router.init] new ' + access + ' route-module: ' + module.pluginName + '/' + route);
                if(!loadedModules[module.pluginName]) {
                    loadedModules[module.pluginName] = module;
                    // loadedModules[module.pluginName] = new module(contentElement, options);
                }
                routes[route] = loadedModules[module.pluginName];
            });
        },

        setup: function(el, options) {
            if(options.modules) {
                this.constructor.modules = options.modules;
                delete options.modules;
            }
            
            return can.Control.prototype.setup.apply(this, arguments);
        },

        init: function(el, options) {
            console.log('[Router.init] organize routes');
            this.contentElement = $('#content', el);

            var routes = {}, 
                loadedModules = {};

            if(this.constructor.modules && this.constructor.modules.public) {
                //setup routes, that are available without login
                this._initRoutes('public', routes, loadedModules, this.contentElement, options);
            }

            if(this.constructor.modules && this.constructor.modules.private) {
                //setup routes, that are only available with login
                if(userAccess() === 'private') {
                    // user is already authed
                    this._initRoutes('private', routes, loadedModules, this.contentElement, options);                    
                } else {
                    // user has to auth first
                    userAccess.bind('change', this.proxy(function(ev, newVal, oldVal) {
                        console.log('userAccess changed', arguments);
                        if(newVal === 'private') {
                            this._initRoutes('private', routes, loadedModules, this.contentElement, options);
                            
                            if(savedRoute) {
                                console.log('[Router] restoring: %O', savedRoute);
                                can.route.attr(savedRoute);
                            } else if(!can.route.attr() || !can.route.attr('view') || can.route.attr('view') == 'login') {
                                console.warn(can.route.attr('view'));
                                can.route.attr({ view: 'setcards' }, true);
                            }
                        }
                    }));
                }
            }

            this.constructor.routes = routes;

            // get notified, when the user attribute is set in the sate (i.e. on login)
            this.options.state && this.options.state.bind('user', this.proxy(function(ev, newVal, oldVal) {
                console.log('USER changed', arguments);

                if(newVal && !oldVal) {
                    userAccess('private');
                }
            }));

            console.log('[Router] init calling can.route.ready')
            can.route.ready();

            console.log('[Router] init done, route looks like this: ', can.route.attr());

            if(can.isEmptyObject(can.route.attr())) {
                console.log('[Router] route is empty, routing to default view.');

                var def = this.constructor.modules[userAccess()] && this.constructor.modules[userAccess()].default;
                if(def && typeof def === 'string') {

                    location.hash = '#!/' + def;
                    can.route.attr({
                        view: def
                    });
                }
            }
        },
        // =============================== Routing ===============================

        /**
         * Listen for view route changes.
         * Views are things that are modules.
         */
        '{can.route} view change': function(route, ev, attr, how, newVal, oldVal) {
            console.log('[Router] view changed: ' + (oldVal?oldVal+' -> ':'') + newVal, arguments);

            if(newVal === oldVal && activeModule) {
                console.log('[Router] This is just so that can.route re-evalutes itself.');
                return;
            }

            if(oldVal && newVal && activeModule && activeModule.constructor === this.constructor.routes[newVal]) {
                console.log('[Router] The module did not change. (the route may have)');
                return;
            }

            console.log('[Router] ', this.constructor.routes[newVal]?'has module':'does not have module');
            
            if(oldVal && activeModule) {
                //unload the old view
                console.log('[Router] unloading ' + oldVal);
                activeModule.destroy();
                activeModule = undefined;
            }

            if(newVal && this.constructor.routes[newVal]) {
                //load the new view
                this.loadModule(newVal);
            } else {
                console.log('[Router] nothing found for ' + newVal);
                if(this.constructor.modules.private && this.constructor.modules.private[newVal]) {
                    // the route actually exists, the user is just not entitled to see it
                    savedRoute = can.route.attr();
                    console.log('[Router] login needed for ' + newVal + ' will later restore: %O', savedRoute);

                    // remove all attrs and set view to login
                    can.route.attr({view: 'login'}, true);
                } else {
                    activeModule = undefined;
                    this.contentElement.html(View404());
                }
            }
        },

        loadModule: function(module) {
            console.log('[Router] loading ' + module);

            can.defer(function() {
                activeModule = new this.constructor.routes[module](this.contentElement, this.options);
                this.options.state && (this.options.state.app = activeModule);
                //this will trigger re-evaluation of all active routes
                can.trigger(can.route.data, 'change', ['view', 'set', module, module]);
            }, this);
        }
//        "{can.route} view": "navigate",
//        navigate: function() {
//            var view = can.route.attr('view');
//            console.log('[Router.navigate] /%s (authd=%s)', view, userAccess());


//            // if we dont have a match
//            if(!this.constructor.routes[view]) {
//                var routeParams = can.route.attr();
//                delete routeParams.route;
//                var p = can.param(routeParams);

//                console.warn('[Router.navigate] no match yet: ', p);

//                this.contentElement.html(View404());
//                return;
//            }

//            return console.log('[Router.navigate] match found, let canjs handle the rest');
//            /*
//            // don't load the same again!
//            if(this.options.state.app
//               && this.options.state.app.constructor === this.constructor.routes[view]
//               && this.options.state.app.constructor.pluginName !== 'miscpage')
//                return console.log('[Router.navigate] same controller');

//            // destroy the old instance, if there
//            this.options.state.app &&
//                this.options.state.app.destroy() && console.log('[Router.navigate] destroyed old controller');

//            // init the plugin on this element
//            var app = this.options.state.app =
//                new this.constructor.routes[view]($('#content', this.element), {
//                    state: this.options.state
//                });
//            console.log('[Router.navigate] Loaded new controller')

//            //fire this very route, if the controller supports it
//            var firstRoute = c; //can.route.attr('route'); //can.route.param(can.route.data.serialize());
//            var firstRouteHandler = app[firstRoute + ' route'];
//            if(firstRouteHandler && typeof firstRouteHandler === 'function') {
//                console.log('[Router.navigate] searching for route ' + firstRoute + ' succeeded, running');
//                firstRouteHandler.call(app);
//            } else if(firstRouteHandler && typeof firstRouteHandler === 'string' && app[firstRouteHandler] && typeof app[firstRouteHandler] === 'function') {
//                console.log('[Router.navigate] searching for route ' + firstRoute + ' succeeded, running app[' + firstRouteHandler + ']');
//                app[firstRouteHandler].call(app);
//            } else {
//                console.log('[Router.navigate] searching for route ' + firstRoute + ' failed');
//            }
//            */
//        },

        
    });

    return r;

});