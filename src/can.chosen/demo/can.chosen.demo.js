define([
        'can',
        'mustache!./views/chosen-dom',
        'mustache!./views/chosen-def',
        'mustache!./views/chosen-dyn'
    ],
    function(can, chosenDomView, chosenDefView, chosenDynView) {
        var handlers = {
            test: function(options, $el, ev) {
                options.attr('vals', $el.data('vals'));
            }
        }
        
        return can.Control.extend('can.chosen.demo', {
            init: function(element, options) {
                console.log('[can.chosen] init');
            },

            '/can.chosen/domOptions route': function() {
                console.log('[can.chosen] can.chosen/domOptions route!');

                var options = window.options = new can.Map({
                    vals: 'Belts;Chairs',
                    handlers: handlers
                });
                this.element.html(chosenDomView(options));
            },

            '/can.chosen/defOptions route': function() {
                console.log('[can.chosen] can.chosen/defOptions route!');

                var $def = new can.Deferred(),
                    options = window.options = new can.Map({
                        vals: 'id-1;id-4',
                        d: {
                            //the deferred needs to be wrapped inside another object
                            // because can.view will not render the view unless all def are resolved
                            def: $def
                        },
                        handlers: can.extend({}, handlers, {
                            resolve: function() {
                                $def.resolve([
                                    {label:'test1', value:'id-1'},
                                    {label:'test2', value:'id-2'},
                                    {label:'test3', value:'id-3'},
                                    {label:'test4', value:'id-4'}
                                ]);
                            },
                            reject: function() {
                                $def.reject('Cannot connect to server');
                            }
                        })
                    });

                this.element.html(chosenDefView(options));
            },

            '/can.chosen/dynOptions route': function() {
                console.log('[can.chosen] can.chosen/dynOptions route!');

                var opts = new can.List([]),
                    options = window.options = new can.Map({
                        vals: 'id-1;id-4',
                        opts: opts,
                        handlers: can.extend({}, handlers, {
                            computeId: function() {
                                opts.replace([
                                    {label:'test1', value:'id-1'},
                                    {label:'test2', value:'id-2'},
                                    {label:'test3', value:'id-3'},
                                    {label:'test4', value:'id-4'}
                                ]);
                            },
                            computeTags: function() {
                                opts.replace([
                                    {label:'Bolts', value:'Bolts'},
                                    {label:'Chairs', value:'Chairs'},
                                    {label:'Lamps', value:'Lamps'}
                                ]);
                            }
                        })
                    });

                this.element.html(chosenDynView(options));
            }
        })
    }
)