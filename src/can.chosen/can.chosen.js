/*!
 * can.chosen - 0.0.1
 * https://www.simpletechs.net
 * Copyright (c) 2013 simpleTechs.net (fabian@simpletechs.net)
 * Licensed TBD
 * Uses: canjs (http://canjs.us/), jQuery (http://jquery.com), jQuery.chosen (http://harvesthq.github.io/chosen/)
 * State: IN DEVELOPMENT
 */

requirejs.config({
    "shim": {
        "../../lib/chosen/chosen.jquery": ["jquery"]
    }
});

define([
		'jquery',
		'can',

		'../can.function/can.function',
		'../../lib/chosen/chosen.jquery'
	],
	function($, can) {
		var template = '\
			<select class="chosen {{class}}"\
				can-value="canValue"\
				{{#multiple}}{{.}}{{/multiple}}\
				{{#data-name}}name="{{.}}"{{/data-name}}\
				{{#data-placeholder}}data-placeholder="{{.}}"{{/data-placeholder}}\
				{{#loading}}disabled{{/loading}}\
			>\
				<option></option>\
				<content/>\
				{{#options}}\
					<option value="{{value}}">{{label}}</option>\
				{{/options}}\
			</select>\
		';

		var scopeObj = {
			'data-name': '@',
			'class': '@',
			'multiple': '@',
			'data-placeholder': '@',
			'data-placeholder-loading': '@',
			'data-create-option': '@',
			chosenOptions: {
				width: '100%',
				allow_single_deselect: true,
				create_option: false
			},
			loading: can.compute(false),
			options: [],
			'update-trigger': can.compute()
		}

		var id = 0, batchNum;
		return can.Component.extend({
			setup: function(el, hookupOptions) {
				// run the supers setup function
				var ret = can.Component.prototype.setup.apply(this, arguments);

				var self = this,
					scope = this.scope,
					$def = this.scope.attr('deferredOptions');

				//copy create_option from data to chosenOptions
				scope.attr('chosenOptions').attr('create_option', scope.attr('data-create-option') == 'true');

				if($def && can.isDeferred($def)) {
					// found a deferred value for options, which means they are not a list.
					// we fix that by getting the value and replacing the options with their real value

					if($def.state() === 'resolved' || $def.state() === 'rejected') {
						// if the deferred is already resolved, we may skip a few steps
						
						$def.then(function(result) {
							self.scope.attr('options').replace(new can.List(can.makeArray(result)));
						}, function(error) {
							self.scope.attr('data-placeholder', error);
						});
					} else {
						// if the deferred is not already resolved, we wait for its change and show
						// the data-placeholder-loading meanwhile
						var placeholder = scope.attr('data-placeholder');
						scope.attr({
							loading: true,
							'data-placeholder': scope.attr('data-placeholder-loading')
						});

						$def.then(function(result) {
							// when the deferred resolved, we update the chosen again
							// so that it shows the correct placeholder and correct options

							can.batch.start();
								// set the fetched options and replace existing ones
								scope.attr('options').replace(new can.List(can.makeArray(result)));

								// update the loading status and the placeholder
								scope.attr({
									loading: false,
									'data-placeholder': placeholder
								});

								// trigger a change on canValue, so that can.view.bindings will
								// update the value. This is needed, becaue the value may have included options
								// that were not avaiable before
								var old = scope.attr('canValue');
								if(old.attr) old = old.attr();
								console.log('Update on canValue', old)
								can.trigger(scope, 'change', ['canValue', 'set', old, old]);
							can.batch.stop();
						}, function(error) {
							can.batch.start();
								// update the loading status and the placeholder
								scope.attr({
									loading: false,
									'data-placeholder': error
								});
								scope.attr({
									loading: true
								})
							can.batch.stop();
						});
					}
				}

				// return supers setup return value.
				return ret;
			},
			tag: "chosen-select",
			template: template,
			scope: scopeObj,
			events: {
				init: function(el, options) {
					this.id = id++;
					// console.log('['+this.id+'] init. val: ' + this.scope.attr('canValue'));
					
					// setup two-way binding, by listening on changes in canValue and updating the
					// chosen accordingly
					this.scope.bind('canValue', this.proxy(function(ev) {
						// console.log('['+this.id+'] canValue changed. val: ' + this.scope.attr('canValue'));
						this.update(undefined, ev);
					}));
				},

				destroy: function() {
					this.scope.unbind('canValue');
				},

				inserted: function() {
					console.log('['+this.id+'] inserted into DOM w/ options: ', this.scope.serialize().chosenOptions||{});
					
					// we have to wait for the DOM to show the options,
					// thus we need a setTimeout here
					setTimeout(this.proxy(function() {
						// this may happen if we are destroyed before the timeout fires
						// but if we were to call $('select.chosen', null) that would select *all* elements on the page
						if(!this.element) return;

						this.$chosen = $('select.chosen', this.element);
						// before actually building the chosen, we have to set the value on our select element
						// this.$chosen.val(this.scope.attr('canValue')&&this.scope.attr('canValue'));

						this._createChosen();
					}), 0);
				},

				_createChosen: function(destroy) {
					if(!this.$chosen) {
						return console.warn('Chosen was not yet initialized');
					}
					if(destroy) {
						this.$chosen.chosen('destroy');
					}
					var val = this.scope.attr('canValue')&&this.scope.attr('canValue').split(';');
					this._createOptions(val);
					this.$chosen.val(val).chosen(this.scope.serialize().chosenOptions||{});
				},

				_createOptions: function(val) {
					if(!val || !val.length || !this.scope.attr('chosenOptions.create_option')) {
						return;
					}
					var $chosen = this.$chosen,
						options = $('option', $chosen);

					val.forEach(function(v) {
						var exists = can.filter(options, function(i, o) {
							return o.value === v;
						}).length > 0;

						if(!exists) {
							console.log('[can.chosen]: No option for this value, creating:', v);
							$chosen.append('<option value="' + v + '">' + v + '</option>');
						}
					});
				},
				
				// when the options change, we have to update the chosen, to reflect the new options
				'{updateTrigger} change': 'update',
				'{options} change': 'update',
				'update': function(_, ev) {
					if(ev.batchNum === undefined || ev.batchNum !== batchNum ) {
						batchNum = ev.batchNum;
						
						console.warn('['+this.id+'] the options/value changed, re-render the select');
						
						// we have to wait for the DOM to show the new options,
						// thus we need a setTimeout here
						setTimeout(this.proxy(function() {
							if(!this.$chosen) return;

							var val = this.scope.attr('canValue')&&this.scope.attr('canValue').split(';');
							this._createOptions(val);
							this.$chosen.val(val).trigger("chosen:updated");
						}), 0);
					} else {
						console.debug('Skipped update due to same batchNum');
					}
				},

				// in order to have chosen reload the placeholder-text, we need to 
				// destroy and then rebuild the whole chosen-object
				'{loading} change': 'recreate',
				'recreate': function(_, ev) {
					if(ev.batchNum === undefined || ev.batchNum !== batchNum ) {
						batchNum = ev.batchNum;
						console.warn('['+this.id+'] the loading state changed, re-create the select');
						
						// recreate chosen by destroying it first
						this._createChosen(true);
					} else {
						console.debug('Skipped recreate due to same batchNum');
					}
				}
			},
			helpers: {

			}
		});

});